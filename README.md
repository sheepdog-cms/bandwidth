# Bandwidth Extension
**This extension is bundled, preconfigured and enabled in official Sheepdog builds.**

Serves statically compressed assets to provide the best performance,
bandwidth usage and browser support.

Automatic upload compression with [matt support](https://gitlab.com/sheepdog-cms/matt/server).


## Installation
### Official bundle

* Nothing to do! It's already installed! And configured!

### From source
**If you just have cloned the core repository:**

* `git submodule update --init --recursive`

**If it's third-party distribution:**

* If you've got /extensions/Bandwidth directory (and it's not empty), then everything is set!
  You may need to enable the extension and/or patch your server's configuration.

* 

## Matt integration
We host free matt API, that is available under **matt.cos.ovh**.

Unregistered access allows up to 25 requests per month. [Get an API token](https://matt.cos.ovh/request-access) that lets you request
up to 10000 requests per month. The API token is bound to a domain name and up to 4 IPs. You don't know your server's
IP? No problem, the domain name is enough!

Self-hosting is not a problem if you're familiar with Docker.