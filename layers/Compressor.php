<?php
/*
 * Compressor
 *
 *
 * @package Sheepdog
 * @since   0.2.0
*/
namespace Sheepdog\Extensions\Bandwidth;

class AfterUploadWrite {
    /*
     * GZips uploaded file after its written.
     *
     * @return null
    */
    public function Run ($Filename) {
        global $config;
        $content = file_get_contents($Filename);
        $gzcontent = gzencode($content, 9);
        file_put_contents($Filename . '.gz', $gzcontent);
        if ($config['debug']) {
            echo 'Sheepdog-Bandwidth: compressed.';
        }
    }
}